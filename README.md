# Side Scroller (Project 4)
Marvin Vinluan (mvinlua1@binghamton.edu)  
21 June 2017

Adapting from the side scroller tutorial at https://www.raywenderlich.com/62049/sprite-kit-tutorial-make-platform-game-like-super-mario-brothers-part-1

Touching left side of screen moves the koala forward; touching right side of screen makes koala jump.  Not able to make both touch controls work concurrently/independently; for now you have to release one side before the other will work.

Experimented with adapting the tutorial code using the external Tiled level editor, seeing which system fonts were available to use, physics calculations, and a bit of unsuccessful digging into why a view might not remove from superview reliably.

Additionally: In this project I learned how to set up my own phone to accept programs directly from Xcode, instead of relying entirely on the Simulator app on my Mac.